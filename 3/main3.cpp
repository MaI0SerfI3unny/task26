
#include <iostream>

class Monitor {
private:
    int width;
    int height;
    int x;
    int y;
public:
    Monitor(int w, int h, int x, int y) : width(w), height(h), x(x), y(y) {}

    void move(int dx, int dy) {
        x += dx;
        y += dy;
    }

    void resize(int w, int h) {
        width = w;
        height = h;
    }

    void display() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i + y >= 0 && i + y < 50 && j + x >= 0 && j + x < 80) {
                    std::cout << "1";
                } else {
                    std::cout << "0";
                }
            }
            std::cout << std::endl;
        }
    }
};

int main() {
    Monitor monitor(80, 50, 0, 0);
    std::string command;
    while (true) {
        std::cin >> command;
        if (command == "move") {
            int dx, dy;
            std::cin >> dx >> dy;
            monitor.move(dx, dy);
            std::cout << "Moved to (" << dx << ", " << dy << ")" << std::endl;
        } else if (command == "resize") {
            int w, h;
            std::cin >> w >> h;
            monitor.resize(w, h);
            std::cout << "Resized to (" << w << ", " << h << ")" << std::endl;
        } else if (command == "display") {
            monitor.display();
        } else if (command == "close") {
            break;
        }
    }
    return 0;
}