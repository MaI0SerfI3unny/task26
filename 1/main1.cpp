#include <iostream>
#include <list>
#include <ctime>
using namespace std;

class Track 
{
public:
    Track(string name, tm creationDate, int duration) 
    {
        this->name = name;
        this->creationDate = creationDate;
        this->duration = duration;
    }

    string getName() const
    {
        return name;
    }

    tm getCreationDate() const 
    {
        return creationDate;
    }

    int getDuration() const 
    {
        return duration;
    }

private:
    string name;
    tm creationDate;
    int duration;
};

class AudioPlayer 
{
public:
    AudioPlayer() {}

    void addTrack(Track track) 
    {
        tracks.push_back(track);
    }

    void play() 
    {
        string name;
        cout << "Input the name of the track: ";
        cin >> name;

        for (auto track : tracks) 
        {
            if (track.getName() == name) 
            {
                cout << "Now playing: \n\tName: " << track.getName() 
                    << "\n\tCreation Date: " << track.getCreationDate().tm_year
                    << "\n\tDuration: " << track.getDuration() << endl;
            }
        }
    }

    void pause() 
    {
        cout << "Track paused." << endl;
    }

    void next() 
    {
        int randomIndex = rand() % tracks.size();
        auto track = std::next(tracks.begin(), randomIndex);

        cout << "Now playing: \n\tName: " << track->getName()
            << "\n\tCreation Date: " << track->getCreationDate().tm_year
            << "\n\tDuration: " << track->getDuration() << endl;
    }

    void stop() 
    {
        cout << "Track stopped." << endl;
    }

private:
    list<Track> tracks;
};

int main() 
{
    AudioPlayer player;
    tm trackDateFirst = {0, 0, 0, 1, 1, 2020};
    tm trackDateSecond = {0, 0, 0, 2, 1, 2020};
    tm trackDateThird = {0, 0, 0, 3, 1, 2020};
    Track track1("Track1", trackDateFirst, 300);
    Track track2("Track2", trackDateSecond, 180);
    Track track3("Track3", trackDateThird, 240);

    player.addTrack(track1);
    player.addTrack(track2);
    player.addTrack(track3);

    while (true) 
    {
        string command;
        cin >> command;
        if (command == "play") player.play();
        else if (command == "pause") player.pause();
        else if (command == "next") player.next();
        else if (command == "stop") player.stop();
        else if (command == "exit") break;
        else cout << "Invalid command!" << endl;
        
    }
    return 0;
}