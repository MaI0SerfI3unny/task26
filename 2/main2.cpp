#include <iostream>
#include <map>
using namespace std;

class PhoneNumber {
    public:
        string number;
        PhoneNumber(string number) {
            this->number = number;
        }
};

class AddressBookEntry {
public:
    string name;
    PhoneNumber* phoneNumber;
    AddressBookEntry(string name, PhoneNumber* phoneNumber) {
        this->name = name;
        this->phoneNumber = phoneNumber;
    }
};

class MobilePhone {
private:
    map<string, AddressBookEntry*> addressBook;
public:
    void add(string name, PhoneNumber* phoneNumber) {
        addressBook[name] = new AddressBookEntry(name, phoneNumber);
    }
    void call(string nameOrNumber) {
        if (addressBook.find(nameOrNumber) != addressBook.end()) {
            cout << "CALL " << addressBook[nameOrNumber]->phoneNumber->number << endl;
        } else {
            cout << "CALL " << nameOrNumber << endl;
        }
    }
    void sms(string nameOrNumber, string message) {
        if (addressBook.find(nameOrNumber) != addressBook.end()) {
            cout << "SMS to " << addressBook[nameOrNumber]->phoneNumber->number << ": " << message << endl;
        } else {
            cout << "SMS to " << nameOrNumber << ": " << message << endl;
        }
    }
};

int main() {
    MobilePhone phone;
    while (true) {
        string command;
        getline(cin, command);
        if(command == "add") {
            string name;
            string number;
            cout<<"For adding you must type name and number"<<endl;
            getline(cin,name);
            getline(cin,number);
            phone.add(name, new PhoneNumber(number));
        }else if (command == "call") {
            string nameOrNumber;
            cout<<"Type name what you wanna call"<<endl;
            getline(cin, nameOrNumber);
            phone.call(nameOrNumber);
        } else if (command == "sms") {
            string nameOrNumber;
            string message;
            cout<<"For sending sms you need type name and message"<<endl;
            getline(cin, nameOrNumber);
            getline(cin, message);
            phone.sms(nameOrNumber, message);
        }else if (command == "exit"){ break; }
    }
    return 0;
}